package fr.istic.metronome.client;

import fr.istic.metronome.command.*;
import fr.istic.metronome.engine.*;
import fr.istic.metronome.ihm.*;

/**
 * 
 * Contr�leur principal
 * 
 */

public class Main {
	
	private static MetronomeEngineImpl me;
	private static HorlogeImpl horloge;
	private static IHMImpl ihm;
	private static IBeeper beeper;
	private static ISlider slider;
	private static ILed beatLed;
	private static ILed barLed;
	private static IButton boutonStart;
	private static IButton boutonStop;
	private static IButton boutonInc;
	private static IButton boutonDec;
	
	/**
	 * Instancie l'application
	 */
	public Main(){
		ihm = new IHMImpl();
		me = new MetronomeEngineImpl();
		
		horloge = new HorlogeImpl();
		me.setHorloge(horloge);
		
		beeper = new BeeperImpl();
		
		boutonStart = ihm.getBoutonStart();
		boutonStop = ihm.getBoutonStop();
		boutonInc = ihm.getBoutonInc();
		boutonDec = ihm.getBoutonDec();
		slider = ihm.getSlider();
		
		beatLed = ihm.getBeatLed();
		beatLed.setHorloge(horloge);
		
		barLed = ihm.getBarLed();
		barLed.setHorloge(horloge);
	}
	
	/**
	 * Lie tout les �l�ments de l'appliation (ihm, commandes...)
	 * @param args
	 */
	public static void main(String[]  args){
		
		Main ctl = new Main();
		
		// CMD
		
		ICommand startcmd = new StartEventImpl(ctl);
		ICommand stopcmd = new StopEventImpl(ctl);
		ICommand beatcmd = new BeatEventImpl(ctl);
		ICommand barcmd = new BarEventImpl(ctl);
		ICommand slidercmd = new SliderEventImpl(ctl);
		ICommand tempocmd = new TempoEventImpl(ctl);
		ICommand inccmd = new BeatPerBarIncImpl(ctl);
		ICommand deccmd = new BeatPerBarDecImpl(ctl);
		
		ICommand turnoffbeatledcmd = new TurnOffLedImpl(beatLed);
		ICommand turnoffbarledcmd = new TurnOffLedImpl(barLed);
		
		me.setBeatCmd(beatcmd);
		me.setBarCmd(barcmd);
		me.setTempoCmd(tempocmd);
		
		slider.setSliderCmd(slidercmd);
		beatLed.setLedCmd(turnoffbeatledcmd);
		barLed.setLedCmd(turnoffbarledcmd);
		boutonStart.setButtonCmd(startcmd);
		boutonStop.setButtonCmd(stopcmd);
		boutonInc.setButtonCmd(inccmd);
		boutonDec.setButtonCmd(deccmd);
		
	}

	/**
	 * Demarre l'application
	 */
	public static void start() {
		me.setRunning(true);
	}
	
	/**
	 * Arrete l'application
	 */
	public static void stop(){
		me.setRunning(false);
	}

	/**
	 * Declenche un evenement beat
	 */
	public void handleBeatEvent() {
		beatLed.allumerLED();
		beeper.beep();
	}
	
	/**
	 * Declenche un evenement Bar
	 */
	public void handleBarEvent() {
		barLed.allumerLED();
		beeper.beep();
	}
	
	// ------- TEMPO ------------ //
	
	/**
	 * Setter tempo
	 * @param t
	 */
	public void setTempo(int t){
		me.setTempo(t);
	}
	
	/**
	 * Modifie le tempo
	 */
	public void tempoChanged() {
		int tempo = me.getTempo();
		
		// reset led
		beatLed.eteindreLED();
		barLed.eteindreLED();
	
		ihm.setTempoView(tempo);
	}
	
	/**
	 * Augmente le nombre de beat par mesure
	 */
	public void BpmInc(){
		int bpb = me.getBeatPerBar();
		if(bpb < 7){
			bpb++;
			me.setBeatPerBar(bpb);
		}
		
	}

	/**
	 * Diminue le nombre de beat par mesure
	 */
	public void BpmDec() {
		int bpb = me.getBeatPerBar();
		if(bpb > 2){
			bpb--;
			me.setBeatPerBar(bpb);
		}
	}
	
	// -------------------------//
	
	// -------  SLIDER  ----------//
	
	/**
	 * S'active quand le slider a �t� d�plac�
	 */
	public void sliderChanged() {
		float pos = slider.position();
		
		int tempo = (int) (pos);

		me.setTempo(tempo);
	}
	
	// ---------------------------
	

}

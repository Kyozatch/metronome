package fr.istic.metronome.ihm;

import javax.swing.JButton;

import fr.istic.metronome.command.ICommand;

/**
 * Interface IButton
 */
public interface IButton {

	/**
	 * Setter buttonCmd
	 * @param cmd
	 * Lie le boutton � une nouvelle commande
	 */
	void setButtonCmd(ICommand cmd);
}

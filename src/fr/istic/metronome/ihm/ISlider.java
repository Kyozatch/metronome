package fr.istic.metronome.ihm;

import fr.istic.metronome.command.ICommand;

/**
 * 
 * Interface ISlider
 *
 */
public interface ISlider {
	
	/**
	 * Renvoie la position du slider
	 * @return
	 * la position du slider
	 */
	float position();
	
	/**
	 * Setter slider Cmd
	 * @param c
	 * la commande a lier
	 */
	void setSliderCmd(ICommand c);
	void setMinorTickSpacing(int i);
	void setMajorTickSpacing(int i);
	void setPaintTicks(boolean b);
	void setPaintLabels(boolean b);
	Object createStandardLabels(int i);
	void setBounds(int i, int j, int k, int l);
	void setLabelTable(Object createStandardLabels);
	ICommand getSliderCmd();
	
}

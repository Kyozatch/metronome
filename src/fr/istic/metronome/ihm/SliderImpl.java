package fr.istic.metronome.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSlider;

import fr.istic.metronome.command.ICommand;

/**
 * 
 * Class Slider
 *
 */
public class SliderImpl extends JSlider implements ISlider, ActionListener {

	private static final long serialVersionUID = 1L;
	
	private ICommand slidercmd;
	
	/**
	 * Constructeur
	 */
	public SliderImpl(int args0,int args1,int args2,int args3){
		super(args0, args1, args2, args3);
		slidercmd = null;
	}
	 
	/**
	 * Renvoie la position du slider
	 * @return
	 * la position du slider
	 */
	public float position() {
		return getValue();
	}

	/**
	 * Setter slider Cmd
	 * @param c
	 * la commande a lier
	 */
	public void setSliderCmd(ICommand c) {
		// TODO Auto-generated method stub
		this.slidercmd = c;
	}
	
	/**
	 * Getter slider cmd
	 * @return slidercmd
	 * La commande du slider
	 */
	public ICommand getSliderCmd() {
		return slidercmd;
	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setLabelTable(Object createStandardLabels) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}

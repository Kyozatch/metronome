package fr.istic.metronome.ihm;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * 
 * Class IHM
 * Extend JFrame
 *
 */
public class IHMImpl extends JFrame implements IIHM{

	private static final long serialVersionUID = 1L;
	
	private ButtonImpl boutonStart;
	private ButtonImpl boutonStop;
	private ButtonImpl boutonInc;
	private ButtonImpl boutonDec;
	private SliderImpl slider;
	private LedImpl beatLed;
	private LedImpl barLed;
	private JLabel lblNewLabel;
	
	/**
	 * Met � jour l'afficheur de tempo
	 * @param tempo
	 * le tempo � afficher
	 */
	public void setTempoView(int tempo) {
		lblNewLabel.setText(tempo+"");
	}
	
	/**
	 * Constructeur
	 */
	public IHMImpl(){
		slider = new SliderImpl(JSlider.HORIZONTAL, 0, 240, 120);
		beatLed = new LedImpl();
		barLed = new LedImpl();
		boutonStart = new ButtonImpl("Start");
		boutonStop = new ButtonImpl("Stop");
		boutonInc = new ButtonImpl("Inc");
		boutonDec = new ButtonImpl("Dec");
		this.lblNewLabel = new JLabel("120");
		
		initialize();
	}
	
	/**
	 * Getter buttonStart
	 * @return
	 * Retourne le bouton
	 */
	public ButtonImpl getBoutonStart() {
		return boutonStart;
	}

	/**
	 * Setter buttonStart
	 * @param boutonStart
	 * boutton a changer
	 */
	public void setBoutonStart(ButtonImpl boutonStart) {
		this.boutonStart = boutonStart;
	}

	/**
	 * Getter buttonStop
	 * @return
	 * Retourne le bouton
	 */
	public ButtonImpl getBoutonStop() {
		return boutonStop;
	}

	/**
	 * Setter buttonStop
	 * @param boutonStop
	 * boutton a changer
	 */
	public void setBoutonStop(ButtonImpl boutonStop) {
		this.boutonStop = boutonStop;
	}

	/**
	 * Getter buttonInc
	 * @return
	 * Retourne le bouton
	 */
	public ButtonImpl getBoutonInc() {
		return boutonInc;
	}

	/**
	 * Setter buttonInc
	 * @param boutonInc
	 * boutton a changer
	 */
	public void setBoutonInc(ButtonImpl boutonInc) {
		this.boutonInc = boutonInc;
	}

	/**
	 * Getter buttonDec
	 * @return
	 * Retourne le bouton
	 */
	public ButtonImpl getBoutonDec() {
		return boutonDec;
	}

	/**
	 * Setter buttonDec
	 * @param boutonDec
	 * boutton a changer
	 */
	public void setBoutonDec(ButtonImpl boutonDec) {
		this.boutonDec = boutonDec;
	}

	/**
	 * Getter slider
	 * @return
	 * le slider actuel
	 */
	public SliderImpl getSlider() {
		return slider;
	}

	/**
	 * Setter slider
	 * @param slider
	 * le nouveau slider
	 */
	public void setSlider(SliderImpl slider) {
		this.slider = slider;
	}

	/**
	 * Gettter beat led
	 * @return
	 * la beat led actuelle
	 */
	public LedImpl getBeatLed() {
		return beatLed;
	}

	/**
	 * Setter beatLed
	 * @param beatLed
	 * la nouvelle beat led
	 */
	public void setBeatLed(LedImpl beatLed) {
		this.beatLed = beatLed;
	}
	/**
	 * Gettter bar led
	 * @return
	 * la bar led actuelle
	 */
	public LedImpl getBarLed() {
		return barLed;
	}

	/**
	 * Setter barLed
	 * @param barLed
	 * la nouvelle bar led
	 */
	public void setBarLed(LedImpl barLed) {
		this.barLed = barLed;
	}

	/**
	 * Parametre l'IHM
	 */
	public void initialize() {
		this.setTitle("Version 1 du metronome"); 
		this.setBounds(100, 100, 436, 206);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		JPanel FenetreDeContenu = new JPanel();
		FenetreDeContenu.setBackground(UIManager.getColor("Button.background"));
		FenetreDeContenu.setLayout(null);
		this.setContentPane(FenetreDeContenu);

		boutonStart.setBounds(29, 128, 72, 23);
		FenetreDeContenu.add((Component) boutonStart);

		boutonStop.setBounds(125, 128, 72, 23);
		FenetreDeContenu.add((Component) boutonStop);

		boutonInc.setBounds(222, 128, 72, 23);
		FenetreDeContenu.add((Component) boutonInc);

		boutonDec.setBounds(318, 128, 72, 23);
		FenetreDeContenu.add((Component) boutonDec);

		slider.setMinorTickSpacing(10);
		slider.setMajorTickSpacing(60);
		slider.setPaintTicks(true);
		slider.setPaintLabels(false);
		slider.setLabelTable(slider.createStandardLabels(10));
		slider.setBounds(29, 50, 135, 45);
		FenetreDeContenu.add((Component) slider);

		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setBorder(BorderFactory.createLineBorder(new Color(120,120,120)));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 31));
		lblNewLabel.setOpaque(true);
		lblNewLabel.setBounds(189, 50, 143, 39);
		FenetreDeContenu.add(lblNewLabel);

		beatLed.setBounds(369, 27, 21, 23);
		beatLed.setBorder(BorderFactory.createLineBorder(new Color(0,0,0)));
		beatLed.setOpaque(true);
		beatLed.setBackground(Color.white);
		FenetreDeContenu.add((Component) beatLed);
		
		barLed.setBounds(369, 66, 21, 23);
		barLed.setBorder(BorderFactory.createLineBorder(new Color(0,0,0)));
		barLed.setOpaque(true);
		barLed.setBackground(Color.white);
		FenetreDeContenu.add((Component) barLed);
		
		// -----------  ACTION LISTENER -----------  //
		
		slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
            	slider.getSliderCmd().execute();
            }
        });
		
		boutonStart.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
                boutonStart.getButtonCmd().execute();
            }
        });
		
		boutonStop.addActionListener(new ActionListener() {
			 
            public void actionPerformed(ActionEvent e)
            {
                boutonStop.getButtonCmd().execute();
            }
        });
		
		boutonInc.addActionListener(new ActionListener() {
			 
            public void actionPerformed(ActionEvent e)
            {
                boutonInc.getButtonCmd().execute();
            }
        });
		
		boutonDec.addActionListener(new ActionListener() {
			 
            public void actionPerformed(ActionEvent e)
            {
                boutonDec.getButtonCmd().execute();
            }
        });

		// ----------------------------------------- //
		
		this.setVisible(true);
		
	}
	
}

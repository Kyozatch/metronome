package fr.istic.metronome.ihm;

import java.awt.Toolkit;

/**
 * 
 * Class Beeper
 *
 */
public class BeeperImpl implements IBeeper{

	/**
	 * Lance un beep sonore
	 */
	public void beep() {
		 Toolkit.getDefaultToolkit().beep();
	}

}

package fr.istic.metronome.ihm;

import javax.swing.border.Border;

import fr.istic.metronome.command.ICommand;
import fr.istic.metronome.engine.HorlogeImpl;
import fr.istic.metronome.engine.MetronomeEngineImpl;

/**
 * 
 * Interface ILed
 *
 */
public interface ILed {
	
	// Controle les LEDs
	/**
	 * allume la led
	 */
	void allumerLED();
	/**
	 * eteind la led
	 */
	void eteindreLED();
	
	/**
	 * Setter commande
	 * @param c
	 * La commande a lier a la Led
	 */
	void setLedCmd(ICommand c);

	/**
	 * Setter horloge
	 * @param c
	 * L'horloge a lier a la Led
	 */
	void setHorloge(HorlogeImpl horloge);
}

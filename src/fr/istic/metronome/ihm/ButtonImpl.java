package fr.istic.metronome.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import fr.istic.metronome.command.ICommand;

/**
 * 
 * Class Boutton
 * Extend JButton
 */
public class ButtonImpl extends JButton implements IButton, ActionListener {
	
	private static final long serialVersionUID = 1L;
	private ICommand buttonCmd;

	/**
	 * Constructeur
	 * @param name
	 * nom du boutton
	 */
	public ButtonImpl(String name){
		super(name);
		setButtonCmd(null);
	}
	
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Getter ButtonCmd
	 * @return
	 * la commande li�e au boutton
	 */
	public ICommand getButtonCmd() {
		return buttonCmd;
	}

	/**
	 * Setter buttonCmd
	 * @param buttoncmd
	 * Lie le boutton � une nouvelle commande
	 */
	public void setButtonCmd(ICommand buttoncmd) {
		this.buttonCmd = buttoncmd;
	}

	
	
}

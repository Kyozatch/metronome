package fr.istic.metronome.ihm;

/**
 * 
 * interface IBeeper
 *
 */
public interface IBeeper {

	/**
	 * Fait un beep sonore
	 */
	void beep();

}
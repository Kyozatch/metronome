package fr.istic.metronome.ihm;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import fr.istic.metronome.command.ICommand;
import fr.istic.metronome.engine.HorlogeImpl;
import fr.istic.metronome.engine.IHorloge;
import fr.istic.metronome.engine.MetronomeEngineImpl;

/**
 * 
 * Class Led
 *
 */
public class LedImpl extends JLabel implements ILed, ActionListener{
	
	private ICommand turnOffCmd;
	private IHorloge horloge;
	
	public LedImpl(){
		super();
		turnOffCmd = null;
	}
	
	public void setHorloge(HorlogeImpl horloge){
		this.horloge = horloge;
	}

	/**
	 * allume la led
	 */
	public void allumerLED() {
		this.setBackground(Color.yellow);
		
		horloge.activerApresDelai(turnOffCmd, (float) 100);
	}

	/**
	 * eteind la led
	 */
	public void eteindreLED() {
		this.setBackground(Color.white);
	}

	/**
	 * Setter commande
	 * @param c
	 * La commande a lier a la Led
	 */
	public void setLedCmd(ICommand c) {
		turnOffCmd = c;
	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}

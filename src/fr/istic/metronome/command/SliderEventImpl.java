package fr.istic.metronome.command;

import fr.istic.metronome.client.Main;

/**
 * 
 * Commande SliderEvent
 *	
 */
public class SliderEventImpl implements ICommand{
	
	private Main ctl;
	
	/**
	 * Evenement slider boug�
	 */
	public void execute() {
		ctl.sliderChanged();
	}

	/**
	 * Constructeur Cmd
	 * @param ctl
	 */
	public SliderEventImpl(Main ctl) {
		// TODO Auto-generated constructor stub
		this.ctl = ctl;
	}
	
	
}

package fr.istic.metronome.command;

import fr.istic.metronome.client.Main;

/**
 * 
 * Commande BeatParBarDec
 *	
 */
public class BeatPerBarDecImpl implements ICommand{

	private Main ctl;
	
	/**
	 * Evenement perte Beat par mesure 
	 */
	public void execute() {
		ctl.BpmDec();
	}

	/**
	 * Constructeur Cmd
	 * @param ctl
	 */
	public BeatPerBarDecImpl(Main ctl) {
		this.ctl = ctl;
	}

}

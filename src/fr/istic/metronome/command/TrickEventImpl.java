package fr.istic.metronome.command;

import fr.istic.metronome.engine.IMetronomeEngine;
import fr.istic.metronome.engine.MetronomeEngineImpl;

/**
 * 
 * Commande TrickEvent
 *	
 */
public class TrickEventImpl implements ICommand{
	
	private IMetronomeEngine me;
	
	/**
	 * Evenement principal lanc�
	 */
	public void execute() {
		// TODO Auto-generated method stub
		me.trick();
	}

	/**
	 * Constructeur Cmd
	 * @param me
	 */
	public TrickEventImpl(MetronomeEngineImpl me){
		this.me = me;
	}
	
}

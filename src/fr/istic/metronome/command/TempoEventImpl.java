package fr.istic.metronome.command;

import fr.istic.metronome.client.Main;

/**
 * 
 * Commande TempoEvent
 *	
 */
public class TempoEventImpl implements ICommand{
	
	private Main ctl;
	
	/**
	 * Evenement tempo modifi�
	 */
	public void execute() {
		ctl.tempoChanged();
	}
	
	/**
	 * Constructeur Cmd
	 * @param ctl
	 */
	public TempoEventImpl(Main ctl) {
		this.ctl = ctl;
	}

}

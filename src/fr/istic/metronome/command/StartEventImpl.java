package fr.istic.metronome.command;

import fr.istic.metronome.client.Main;

/**
 * 
 * Commande StartEvent
 *	
 */
public class StartEventImpl implements ICommand {

	private Main ctl;
	
	/**
	 * Evenement appli d�marr�e
	 */
	public void execute() {
		ctl.start();
	}
	
	/**
	 * Contructeur Cmd
	 * @param ctl
	 */
	public StartEventImpl(Main ctl) {
		this.ctl = ctl;
	}
	
	
}

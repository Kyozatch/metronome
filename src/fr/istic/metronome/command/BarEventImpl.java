package fr.istic.metronome.command;

import fr.istic.metronome.client.Main;

/**
 * 
 * Commande BarEvent
 *	
 */
public class BarEventImpl implements ICommand {
	
	private Main ctl;
	
	/**
	 * Evenement Bar
	 */
	public void execute() {
		ctl.handleBarEvent();
	}
	
	/**
	 * Constructeur Cmd
	 * @param ctl
	 */
	public BarEventImpl(Main ctl) {
		this.ctl = ctl;
	}

}

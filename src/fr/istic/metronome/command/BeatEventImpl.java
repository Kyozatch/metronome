package fr.istic.metronome.command;

import fr.istic.metronome.client.Main;

/**
 * 
 * Commande BeatEvent
 *	
 */
public class BeatEventImpl implements ICommand{

	private Main ctl;
	
	/**
	 * Evenement Beat
	 */
	public void execute() {
		ctl.handleBeatEvent();
	}

	/**
	 * Constructeur Cmd
	 * @param ctl
	 */
	public BeatEventImpl(Main ctl) {
		this.ctl = ctl;
	}

}

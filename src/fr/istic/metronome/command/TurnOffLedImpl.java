package fr.istic.metronome.command;

import fr.istic.metronome.ihm.ILed;

/**
 * 
 * Commande TurnOffLed
 * 
 */
public class TurnOffLedImpl implements ICommand{
	
	private ILed led;

	/**
	 * Constructeur Cmd
	 * @param led
	 */
	public TurnOffLedImpl(ILed led) {
		this.led = led;
	}

	/**
	 * Evenement �teindre la Led
	 */
	public void execute() {
		led.eteindreLED();
	}

}

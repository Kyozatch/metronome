package fr.istic.metronome.command;

/**
 * 
 * Interface Command
 *
 */
public interface ICommand {
	
	/**
	 * fonction principale
	 */
	void execute();
	
}

package fr.istic.metronome.command;

import fr.istic.metronome.client.Main;

/**
 * 
 * Commande StopEvent
 *	
 */
public class StopEventImpl implements ICommand {

	private Main ctl;
	
	/**
	 * Evenement appli stop�e
	 */
	public void execute() {
		ctl.stop();
	}
	
	/**
	 * Contructeur Cmd
	 * @param ctl
	 */
	public StopEventImpl(Main ctl) {
		this.ctl = ctl;
	}

}

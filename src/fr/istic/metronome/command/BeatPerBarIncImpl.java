package fr.istic.metronome.command;

import fr.istic.metronome.client.Main;

/**
 * 
 * Commande BeatParBarInc
 *	
 */
public class BeatPerBarIncImpl implements ICommand{

	private Main ctl;
	
	/**
	 * Evenement gain Beat par mesure 
	 */
	public void execute() {
		ctl.BpmInc();
	}

	/**
	 * Constructeur Cmd
	 * @param ctl
	 */
	public BeatPerBarIncImpl(Main ctl) {
		this.ctl = ctl;
	}

}

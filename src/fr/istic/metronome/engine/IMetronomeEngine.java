package fr.istic.metronome.engine;

import fr.istic.metronome.command.ICommand;

/**
 * 
 * Interface MetronomeEngine
 *
 */
public interface IMetronomeEngine {
	
	/**
	 * Setter BeatCmd
	 * @param c
	 * La commande � lier
	 */
	void setBeatCmd(ICommand c);
	/**
	 * Setter BarCmd
	 * @param c
	 * La commande � lier
	 */
	void setBarCmd(ICommand c);
	/**
	 * Setter TempoCmd
	 * @param c
	 * La commande � lier
	 */
	void setTempoCmd(ICommand c);
	
	/**
	 * Getter tempo
	 * @return le tempo actuel
	 */
	int getTempo();
	/**
	 * Setter tempo
	 * @param t
	 * le nouveau tempo
	 */
	void setTempo(int t);
	
	/**
	 * Getter BeatPerBar
	 * @return
	 * le nombre de beat par mesure actuel
	 */
	int getBeatPerBar();
	/**
	 * Setter BeaPerBar
	 * @param b
	 * le nouveau nombre de beat par mesure
	 */
	void setBeatPerBar(int b);
	
	/**
	 * Determine si l'appli tourne
	 * @return
	 * boolean
	 */
	boolean isRunning();
	/**
	 * Lance ou Stop l'appli
	 * @param on
	 * boolean
	 */
	void setRunning(boolean on);
	/**
	 * Boucle Principale beat/bar
	 */
	void trick();

	
}

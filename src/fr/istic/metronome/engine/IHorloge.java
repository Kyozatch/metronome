package fr.istic.metronome.engine;

import fr.istic.metronome.command.ICommand;

/**
 * 
 * Interface Horloge
 *
 */
public interface IHorloge {
	
	// Appel p�riodiquement l'exec de cmd toutes les 'periodesEnSecondes'
	/**
	 * Execute periodiquement une commande
	 * @param c
	 * Commande a executer periodiquement
	 * @param periodeEnSecondes
	 * A executer tout les "periodeEnSecondes" secondes
	 */
	void activerPeriodiquement(ICommand c, float periodeEnSecondes);
	
	// Appel p�riodiquement l'exec de cmd apr�s un d�lai de 'd�laiEnSecondes'
	/**
	 * Execute apr�s un d�lai une commande
	 * @param c
	 * Commande a executer apr�s un d�lai
	 * @param periodeEnSecondes
	 * A executer apr�s un d�lai de "d�laiEnSecondes" secondes
	 */
	void activerApresDelai(ICommand c, float delaiEnSecondes);
	
	/**
	 * Desactive le Timer et toutes ses t�ches en cours
	 */
	void desactiver(ICommand c);
}

package fr.istic.metronome.engine;

import java.util.Timer;
import java.util.TimerTask;

import fr.istic.metronome.command.ICommand;

/**
 * 
 * Class Horloge
 *
 */
public class HorlogeImpl implements IHorloge {
	
	private Timer timer;
	
	/**
	 * Constructeur
	 */
	public HorlogeImpl() {
		timer = new Timer();
	}

	/**
	 * Execute periodiquement une commande
	 * @param c
	 * Commande a executer periodiquement
	 * @param periodeEnSecondes
	 * A executer tout les "periodeEnSecondes" secondes
	 */
	public void activerPeriodiquement(final ICommand c, float periodeEnSecondes) {
		TimerTask task = new TimerTask()
		{
			@Override
			public void run() 
			{
				c.execute();
			}
		};
		
		timer.scheduleAtFixedRate(task, 0, (long) periodeEnSecondes);
	}

	
	
	/**
	 * Execute apr�s un d�lai une commande
	 * @param c
	 * Commande a executer apr�s un d�lai
	 * @param periodeEnSecondes
	 * A executer apr�s un d�lai de "d�laiEnSecondes" secondes
	 */
	public void activerApresDelai(final ICommand c, float delaiEnSecondes) {
		TimerTask task = new TimerTask()
		{
			@Override
			public void run() 
			{
				c.execute();
			}
		};
		
		timer.schedule(task, (long) (delaiEnSecondes));
	}

	/**
	 * Desactive le Timer et toutes ses t�ches en cours
	 */
	public void desactiver(ICommand c) {
		timer.purge();
		timer.cancel();
		timer = new Timer();
		//c.execute();
	}
	
}

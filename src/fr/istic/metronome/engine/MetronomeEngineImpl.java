package fr.istic.metronome.engine;

import fr.istic.metronome.command.ICommand;
import fr.istic.metronome.command.TrickEventImpl;

/**
 * 
 * Class MetronomeEngine
 *
 */
public class MetronomeEngineImpl implements IMetronomeEngine{
	
	private static ICommand beatCmd;
	private static ICommand barCmd;
	private static ICommand tempoCmd;
	private static ICommand trick;
	
	private static boolean run;
	private static int beatPerBar;
	private static int countBeat;
	private static int tempo;
	private static HorlogeImpl horloge;
	
	/**
	 * Constructeur
	 */
	public MetronomeEngineImpl(){
		beatCmd = null;
		barCmd = null;
		tempoCmd = null;
		trick = null;
		run = false;
		beatPerBar = 4;
		tempo = 120;
		countBeat = 1;
	}
	/**
	 * Setter horloge
	 * @param horloge
	 * la nouvelle horloge
	 */
	public void setHorloge(HorlogeImpl horloge){
		this.horloge = horloge;
	}
	
	/**
	 * Getter horloge
	 * @return
	 * L'horloge actuelle
	 */
	public HorlogeImpl getHorloge(){
		return horloge;
	}
	
	/**
	 * Setter TempoCmd
	 * @param c
	 * La commande � lier
	 */
	public void setTempoCmd(ICommand c) {
		tempoCmd = c;
	}

	/**
	 * Setter BeatCmd
	 * @param c
	 * La commande � lier
	 */
	public void setBeatCmd(ICommand c) {
		beatCmd = c;
	}

	/**
	 * Setter BarCmd
	 * @param c
	 * La commande � lier
	 */
	public void setBarCmd(ICommand c) {
		barCmd = c;
	}

	/**
	 * Getter tempo
	 * @return le tempo actuel
	 */
	public int getTempo() {
		return tempo;
	}

	/**
	 * Setter TempoCmd
	 * @param c
	 * La commande � lier
	 */
	public void setTempo(int t) {
		tempo = t;
		
		// Si le metronome isRunning(), reset le tempo.
		if(isRunning()){
			resetTempo();
		}
		
		tempoCmd.execute();
		
	}
	
	/**
	 * Pr�pare le changement � un nouveau tempo.
	 * Stop le Timer et les TimerTasks.
	 */
	public void resetTempo(){
		// Reinitialise le compte de beat jou�
		countBeat = 1;
		
		// Arrete le TimerTask et reinitialise un chrono
		horloge.desactiver(trick);
		
		float time =  (float)60/tempo;
		
		// Relance un chrono avec le nouveau tempo
		horloge.activerPeriodiquement(trick, time*1000);
	}

	/**
	 * Getter BeatPerBar
	 * @return
	 * le nombre de beat par mesure actuel
	 */
	public int getBeatPerBar() {
		return beatPerBar;
	}

	/**
	 * Setter BeaPerBar
	 * @param b
	 * le nouveau nombre de beat par mesure
	 */
	public void setBeatPerBar(int b) {
		beatPerBar = b;
		countBeat = 1;
		tempoCmd.execute();
	}

	/**
	 * Determine si l'appli tourne
	 * @return
	 * boolean
	 */
	public boolean isRunning() {
		return run;
	}

	/**
	 * Lance ou Stop l'appli
	 * @param on
	 * boolean
	 */
	public void setRunning(boolean on) {
		
		/* Si il y a un changement d'etat applique les changements */
		if(run != on){
			
			run = on;
			
			// Si le metronome est allum�
			if(isRunning()){
				trick = new TrickEventImpl(this);
				float time =  (float)60/tempo;
				horloge.activerPeriodiquement(trick, time*1000);
			}
			
			// sinon
			else{
				horloge.desactiver(trick);
			}
		}
	}
	
	/**
	 * Boucle Principale beat/bar
	 */
	public void trick() {
		if(countBeat % beatPerBar != 0){
			beatCmd.execute();
			countBeat++;
		}
		else{
			beatCmd.execute();
			barCmd.execute();
			countBeat = 1;
		}
	}
}
